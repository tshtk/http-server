package org.example.framework.http;

import org.example.framework.parser.QueryParser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

class RequestTest {
    @Test
    void methodsShouldWorks() {
        String query = "text=java&type=ebook&type=course";

        Map<String, List<String>> expected = new LinkedHashMap<>();
        List<String> text = new ArrayList<>();
        text.add("java");
        List<String> type = new ArrayList<>();
        type.add("ebook");
        type.add("course");
        expected.put("text", text);
        expected.put("type", type);

        Request request = new Request();
        request.setQuery(query);
        Request parsedRequest = QueryParser.parse(request);

        Assertions.assertEquals(Optional.of("ebook"), parsedRequest.getQueryParam("type"));
        Assertions.assertEquals(Optional.empty(), parsedRequest.getQueryParam("value"));

        Assertions.assertIterableEquals(type, parsedRequest.getQueryParams("type"));

        Map<String, List<String>> actual = parsedRequest.getAllQueryParams();

        Assertions.assertIterableEquals(expected.get("text"), actual.get("text"));
        Assertions.assertIterableEquals(expected.get("type"), actual.get("type"));
    }
}
