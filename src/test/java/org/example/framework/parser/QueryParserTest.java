package org.example.framework.parser;

import org.example.framework.http.Request;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class QueryParserTest {
    @Test
    void shouldParse() {
        String query = "text=java&type=ebook&type=course";

        Map<String, List<String>> expected = new LinkedHashMap<>();
        List<String> text = new ArrayList<>();
        text.add("java");
        List<String> type = new ArrayList<>();
        type.add("ebook");
        type.add("course");
        expected.put("text", text);
        expected.put("type", type);

        Request request = new Request();
        request.setQuery(query);
        Request parsedRequest = QueryParser.parse(request);

        Map<String, List<String>> actual = parsedRequest.getQueryParams();
        Assertions.assertIterableEquals(expected.get("text"), actual.get("text"));
        Assertions.assertIterableEquals(expected.get("type"), actual.get("type"));
    }
    @Test
    void shouldNotParse() {

        Request request = new Request();
        Request parsedRequest = QueryParser.parse(request);

        Assertions.assertEquals(request, parsedRequest);
    }
}
