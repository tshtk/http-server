package org.example.framework.middleware;

import org.example.framework.http.Request;

import java.net.Socket;

public interface Middleware {
  void handle(final Socket socket, final Request request);
}
