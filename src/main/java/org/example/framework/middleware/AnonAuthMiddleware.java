package org.example.framework.middleware;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.AnonymousPrincipal;
import org.example.framework.http.Request;

import java.net.Socket;

@Slf4j
public class AnonAuthMiddleware implements Middleware {
  @Override
  public void handle(final Socket socket, final Request request) {
    if (request.getPrincipal() != null) {
      return;
    }

    request.setPrincipal(new AnonymousPrincipal());
  }
}
