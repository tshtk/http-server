package org.example.framework.middleware;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.LoginPrincipal;
import org.example.framework.http.Request;

import java.net.Socket;

@Slf4j
public class SecretAuthNMiddleware implements Middleware {
  @Override
  public void handle(final Socket socket, final Request request) {
    if (request.getPrincipal() != null) {
      return;
    }

    if (request.getHeaders().containsKey("secret-login")) {
      log.debug("contains secret header");
      request.setPrincipal(new LoginPrincipal("admin"));
    }
  }
}
