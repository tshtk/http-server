package org.example.framework.util;

import java.util.HashMap;
import java.util.Map;

public class Maps {
  private Maps() {
  }

  public static <K, V> Map<K, V> of() {
    return new HashMap<>();
  }

  public static <K, V> Map<K, V> of(K key, V value) {
    final HashMap<K, V> map = new HashMap<>();
    map.put(key, value);
    return map;
  }

  public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2) {
    final HashMap<K, V> map = new HashMap<>();
    map.put(key1, value1);
    map.put(key2, value2);
    return map;
  }

  public static <K, V> Map<K, V> of(K key1, V value1, K key2, V value2, K key3, V value3, K key4, V value4) {
    final HashMap<K, V> map = new HashMap<>();
    map.put(key1, value1);
    map.put(key2, value2);
    map.put(key3, value3);
    map.put(key4, value4);
    return map;
  }
}
