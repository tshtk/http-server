package org.example.framework.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class QueryParser {
  private QueryParser() {}

  public static Request parse(final Request request) {
    final String query = request.getQuery();
    if (query == null) {
      return request;
    }
    List<String> values;
    Map<String, List<String>> queryParams = new LinkedHashMap<>();
    String[] queryParts = query.split("&");
    for (String parts : queryParts) {
      int index = parts.indexOf("=");
      String parameter = parts.substring(0, index);
      String value = parts.substring(index+1);
      if (queryParams.containsKey(parameter)) {
        values = queryParams.get(parameter);
      }
      else {
        values = new ArrayList<>();
      }
      values.add(value);
      queryParams.put(parameter, values);
    }
    log.debug("query {} parse to map {}", query, queryParams);
    request.setQueryParams(queryParams);

    return request;
  }
}
