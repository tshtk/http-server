package org.example.framework.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.exception.InvalidRequestStructureException;
import org.example.framework.http.HttpHeaders;
import org.example.framework.http.Request;
import org.example.framework.util.Bytes;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class RequestParser {
    private static final byte[] CRLF = new byte[]{'\r', '\n'};
    private static final byte[] CRLF_CRLF = new byte[]{'\r', '\n', '\r', '\n'};

    public static Request parse(final byte[] buffer) throws UnsupportedEncodingException {

        final int requestLineEndIndex = Bytes.indexOf(buffer, CRLF);
        final int headerStartIndex = requestLineEndIndex + CRLF.length;
        final int headersEndIndex = Bytes.indexOf(buffer, CRLF_CRLF, headerStartIndex);

        if (requestLineEndIndex == -1) {
            throw new InvalidRequestStructureException("request line end not found");
        }
        if (headersEndIndex == -1) {
            throw new InvalidRequestStructureException("header end not found");
        }

        String requestLine = new String(buffer, 0, requestLineEndIndex, StandardCharsets.UTF_8);
        byte[] headers = Arrays.copyOfRange(buffer, headerStartIndex, headersEndIndex + CRLF.length);

        Request parsedRequestLineRequest = parseRequestLine(requestLine);
        Request parsedHeadersRequest = parseHeaders(headers, parsedRequestLineRequest);
        Request parsedRequest = QueryParser.parse(parsedHeadersRequest);
        log.debug("parsed request line and headers: \n{}", parsedRequest);
        return parsedRequest;
    }

    private static Request parseRequestLine(final String requestLine) throws UnsupportedEncodingException {
        String query = null;
        String[] splitRequestLine = requestLine.split("\\s+");
        if (splitRequestLine.length != 3) {
            throw new InvalidRequestStructureException("request line invalid structure");
        }
        String[] pathAndQuery = URLDecoder.decode(splitRequestLine[1], StandardCharsets.UTF_8.name()).split("\\?", 2);
        if (pathAndQuery.length == 2) {
            query = pathAndQuery[1];
        }

        Request request = new Request();
        request.setMethod(splitRequestLine[0]);
        request.setPath(pathAndQuery[0]);
        request.setQuery(query);
        request.setHttpVersion(splitRequestLine[2]);
        return request;
    }

    private static Request parseHeaders(final byte[] bytes, final Request request) {
        Map<String, String> headers = new LinkedHashMap<>();
        int lastProcessedIndex = 0;
        while (lastProcessedIndex < bytes.length - CRLF.length) {
            final int currentHeaderEndIndex = Bytes.indexOf(bytes, CRLF, lastProcessedIndex);
            String currentHeader = new String(bytes, lastProcessedIndex, currentHeaderEndIndex - lastProcessedIndex, StandardCharsets.UTF_8);
            lastProcessedIndex = currentHeaderEndIndex + CRLF.length;
            String[] splitCurrentHeader = currentHeader.split(":\\s*", 2);
            if (splitCurrentHeader.length != 2) {
                throw new InvalidRequestStructureException("current header invalid structure");
            }
            headers.put(splitCurrentHeader[0].toLowerCase(), splitCurrentHeader[1]);
            if (splitCurrentHeader[0].equalsIgnoreCase(HttpHeaders.CONTENT_LENGTH.value())) {
                request.setContentLength(Integer.parseInt(splitCurrentHeader[1]));
            }
        }
        request.setHeaders(headers);
        return request;
    }
}
