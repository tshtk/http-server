package org.example.framework.exception;

public class InvalidRequestLineStructureException extends InvalidRequestStructureException {
  public InvalidRequestLineStructureException() {
  }

  public InvalidRequestLineStructureException(String message) {
    super(message);
  }

  public InvalidRequestLineStructureException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidRequestLineStructureException(Throwable cause) {
    super(cause);
  }

  public InvalidRequestLineStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
