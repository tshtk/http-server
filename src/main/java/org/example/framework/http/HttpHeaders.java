package org.example.framework.http;

public enum HttpHeaders {
  CONTENT_TYPE("Content-Type"),
  CONTENT_LENGTH("Content-Length"),
  ACCEPT("Accept");

  private final String value;

  HttpHeaders(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }
}
